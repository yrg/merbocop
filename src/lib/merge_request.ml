open! Base

module Change = struct
  type t =
    { json: Ezjsonm.value
    ; sha: string
    ; old_path: string
    ; new_path: string
    ; deleted_file: bool
    ; diff: string }
end

module Comment = struct
  type t = {id: int; system: bool; json: Ezjsonm.value}
end

module Branch = struct type t = {name: string; project: Project.t} end

module Commit = struct
  type t =
    { json: Ezjsonm.value
    ; sha: string
    ; title: string
    ; author_email: string
    ; created_at: string
    ; authored_date: string
    ; committed_date: string }
end

module File = struct
  type t =
    {json: Ezjsonm.value; reference: string; file_name: string; content: string}
end

module Approvals = struct
  type t = {approvals_required: int; approved_by: int list}
end

module Reporting = struct
  type t = {mutable conditionals: string list}

  let add_conditional (report : t) s =
    report.conditionals <- s :: report.conditionals
end

type t =
  { id: int
  ; title: string
  ; description: string
  ; project: Project.t
  ; target_branch: Branch.t
  ; source_branch: Branch.t
  ; allow_maintainer_to_push: bool option
  ; wip: bool
  ; last_update: string
  ; (* 2019-10-31T16:51:30.195Z *)
    json: Ezjsonm.value
  ; commits: Commit.t list
  ; changes: Change.t list
  ; comments: Comment.t list option
  ; labels: string list
  ; approvals: Approvals.t
  ; touched_files: File.t list
  ; reports: Reporting.t }

type mr_result =
  (t, [`Mr_not_found of string * string * Ezjsonm.value * Exn.t]) Result.t

let pp_quick ppf =
  let open Fmt in
  function
  | Ok {id; _} -> pf ppf "MR-%d" id
  | Error (`Mr_not_found (_, ci, _)) -> pf ppf "Error@%s" ci

let of_json ?(token : string option) ~(project : string)
    ~(retry_options : Web_api.Retry_options.t) json_obj =
  let open Jq in
  try
    let id = get_field "iid" json_obj |> get_int in
    let comments =
      Option.map
        (token : _ option)
        ~f:(fun tok ->
          let j =
            Gitlab.merge_request_notes ~token:tok ~project ~mr:id ~retry_options
              () in
          try
            get_list
              (fun v ->
                let obj = get_dict v in
                { Comment.id= get_field "id" obj |> get_int
                ; system= get_field "system" obj |> get_bool
                ; json= v } )
              j
          with e ->
            Debug.dbg
              "Gitlab.merge_request_notes project:%s mr:%d\n\n\n%s, %s\n\n"
              project id (Exn.to_string_mach e)
              (Ezjsonm.value_to_string j) ;
            raise e ) in
    let gs s = get_field s json_obj |> get_string in
    let gb s = get_field s json_obj |> get_bool in
    let gi s = get_field s json_obj |> get_int in
    let mr_project = gi "project_id" |> Project.of_id ?token ~retry_options in
    let last_update = gs "updated_at" in
    let title = gs "title" in
    let description = gs "description" in
    let source_branch =
      { Branch.name= gs "source_branch"
      ; project= gi "source_project_id" |> Project.of_id ?token ~retry_options
      } in
    let target_branch =
      { Branch.name= gs "target_branch"
      ; project= gi "target_project_id" |> Project.of_id ?token ~retry_options
      } in
    let commits =
      let json =
        Gitlab.merge_request_commits ?token ~project ~mr:id ~retry_options ()
      in
      try
        get_list
          (fun j ->
            let dict = get_dict j in
            let sha = get_field "id" dict |> get_string in
            let author_email = get_field "author_email" dict |> get_string in
            let title = get_field "title" dict |> get_string in
            let created_at = get_field "created_at" dict |> get_string in
            let authored_date = get_field "authored_date" dict |> get_string in
            let committed_date = get_field "committed_date" dict |> get_string in
            Commit.
              { json
              ; sha
              ; author_email
              ; title
              ; created_at
              ; authored_date
              ; committed_date } )
          json
      with e ->
        Debug.dbg
          "Gitlab.merge_request_commits project:%s mr_id:%d \n\n\n%s, %s\n\n"
          project id (Exn.to_string_mach e)
          (Ezjsonm.value_to_string json) ;
        raise e in
    let changes =
      let json =
        Gitlab.merge_request_changes ?token ~project ~mr:id ~retry_options ()
      in
      try
        let sha = get_dict json |> get_field "sha" |> get_string in
        json |> get_dict |> get_field "changes"
        |> get_list (fun json ->
               let dict = get_dict json in
               let old_path = get_field "old_path" dict |> get_string in
               let new_path = get_field "new_path" dict |> get_string in
               let deleted_file = get_field "deleted_file" dict |> get_bool in
               let diff = get_field "diff" dict |> get_string in
               Change.{json; sha; old_path; new_path; deleted_file; diff} )
      with e ->
        Debug.dbg "Gitlab.merge_request_changes project:%s mr_id:%d\n\n\n%s\n\n"
          project id (Exn.to_string_mach e) ;
        raise e in
    let labels = get_field "labels" json_obj |> get_strings in
    let approvals =
      let json =
        Gitlab.merge_request_approvals ~project ~mr_id:id ~retry_options in
      try
        let dict = get_dict json in
        let approvals_required =
          get_field "approvals_required" dict |> get_int in
        let approved_by =
          get_field "approved_by" dict
          |> get_list (fun json ->
                 get_dict json |> get_field "user" |> get_dict |> get_field "id"
                 |> get_int ) in
        Approvals.{approvals_required; approved_by}
      with e ->
        Debug.dbg
          "Gitlab.merge_request_approvals project:%s mr_id:%d\n\n\n%s\n\n"
          project id (Exn.to_string_mach e) ;
        raise e in
    let touched_files =
      let not_deleted = List.filter changes ~f:(fun c -> not c.deleted_file) in
      List.map not_deleted ~f:(fun chng ->
          (* Debug.dbg "file-change: %s %s\n\n\n%s\n\n" chng.old_path chng.new_path
             chng.diff ; *)
          (chng.sha, chng.new_path) )
      |> List.filter_map ~f:(fun (r, f) ->
             let json =
               Gitlab.mr_file_content ?token ~project ~retry_options
                 ~reference:r f in
             try
               let dict = get_dict json in
               let reference =
                 try get_field "ref" dict |> get_string
                 with e ->
                   Debug.dbg "wrong-file-content: %s %s\n\n\n%s\n\n" f r
                     (Ezjsonm.value_to_string ~minify:false json) ;
                   raise e in
               let file_name = get_field "file_name" dict |> get_string in
               let content = get_field "content" dict |> get_string in
               Some File.{json; reference; file_name; content}
             with e ->
               Debug.dbg
                 "Gitlab.mr_file_content failed to find file:%s at ref:%s \
                  mr_id:%d\n\n\n\
                  %s\n\n"
                 f r id (Exn.to_string_mach e) ;
               raise e ) in
    let reports = Reporting.{conditionals= []} in
    { id
    ; title
    ; description
    ; project= mr_project
    ; comments
    ; commits
    ; changes
    ; last_update
    ; labels
    ; approvals
    ; target_branch
    ; source_branch
    ; touched_files
    ; reports
    ; wip= gb "work_in_progress"
    ; allow_maintainer_to_push=
        (try Some (gb "allow_maintainer_to_push") with _ -> None)
    ; json= `O json_obj }
  with e ->
    Fmt.kstr failwith "Error parsging JSON: %s, %s" (Exn.to_string e)
      (Ezjsonm.value_to_string ~minify:false (`O json_obj))

let most_recent_push mr_result =
  match mr_result with
  | Error _ -> None
  | Ok {commits; _} ->
      List.fold commits ~init:None ~f:(fun prev Commit.{created_at; _} ->
          match prev with
          | None -> Some created_at
          | Some d -> Some (String.max d created_at) )

let find_by_commit ?token ~project ~retry_options commit_sha_prefix : mr_result
    =
  let json = Gitlab.opened_merge_requests ?token ~project ~retry_options () in
  let open Jq in
  List.find_map (get_list get_dict json) ~f:(fun json_obj ->
      if
        String.is_prefix
          (get_field "sha" json_obj |> get_string)
          ~prefix:commit_sha_prefix
      then Some (of_json json_obj ?token ~project ~retry_options)
      else None )
  |> Result.of_option
       ~error:
         (`Mr_not_found
           ( project
           , commit_sha_prefix
           , json
           , Failure (Fmt.str "Cannot find for %s" commit_sha_prefix) ) )

let all_from_project ?token ?wip_status ~retry_options project =
  let json =
    Gitlab.opened_merge_requests ?wip_status ?token ~retry_options ~project ()
  in
  let open Jq in
  List.map (get_list get_dict json) ~f:(fun json_obj ->
      try (project, Ok (of_json json_obj ?token ~project ~retry_options))
      with e ->
        (project, Error (`Mr_not_found (project, "???", `O json_obj, e))) )
