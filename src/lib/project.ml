open! Base

type t =
  { id: int
  ; path_with_namespace: string
  ; http_url_to_repo: string
  ; web_url: string
  ; json: Ezjsonm.value }

let of_id ?token ~retry_options project_id =
  let json = Gitlab.project_by_id ?token project_id ~retry_options in
  let open Jq in
  let sfield name =
    try get_field name (get_dict json) |> get_string
    with e ->
      Fmt.kstr failwith "Gitlab.project_by_id token project_id:%d %s" project_id
        (Exn.to_string_mach e) in
  let path_with_namespace = sfield "path_with_namespace" in
  let http_url_to_repo = sfield "http_url_to_repo" in
  let web_url = sfield "web_url" in
  {id= project_id; path_with_namespace; http_url_to_repo; web_url; json}
